<?php
$pro_features = array();

$pro_features[] = array(
    'title'     => 'Vendor Listing',
    'desc'      => 'View vendor listing with vendor details and earnings.',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/seller@2x.png?58e47e',
    'class'     => 'seller-listing',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

$pro_features[] = array(
    'title'     => 'Commission Per Vendor Report',
    'desc'      => 'View commission per vendor with vendor earnings. You can charge your vendors percentage, giving them an e-commerce solution free of any monthly fees.',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/earn@2x.png?58e47e',
    'class'     => 'commission-per-seller-report',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

$pro_features[] = array(
    'title'     => 'Birds Eye View With Reports',
    'desc'      => 'Every vendor can see his/her own sales report and see a bird eye view on the sales they are making. ',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/reports@2x.png?58e47e',
    'class'     => 'report-bird-view',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

$pro_features[] = array(
    'title'     => 'Coupon Management',
    'desc'      => 'Every vendor manages their own products and discounts they offer. create discount coupons for special sales! ',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/coupon@2x.png?58e47e',
    'class'     => 'coupon-management',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

$pro_features[] = array(
    'title'     => 'Manage Product Reviews',
    'desc'      => 'Each vendor manages their own product reviews independently. Delete, mark as spam or modify the product reviews on the fly.',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/reviews@2x.png?58e47e',
    'class'     => 'manage-product-reviews',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

$pro_features[] = array(
    'title'     => 'Vendor Profile Completeness',
    'desc'      => 'Dokan manage vendors profile completeness par on vendors dashboard. Vendor can view his/her profile completeness percent by the bar. ',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/Dashboard-profile-completion.png?58e47e',
    'class'     => 'profile-completeness',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);


$pro_features[] = array(
    'title'     => 'Vendor Payment Method Setup',
    'desc'      => 'Vendor can manage there payment methods from their dashboard settings. They can set their withdraw method from there.',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/Dashboard-payment.png?58e47e',
    'class'     => 'payment-method',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

$pro_features[] = array(
    'title'     => 'Admin Announcement System for Vendor',
    'desc'      => 'Admin can set announcement for vendors from back-end. Admin can choose all vendor or select individuals as he/she wants. the announcement will then show on vendor dashboard which leads to a announcement list template. ',
    'thumbnail' => 'https://wedevs-com-wedevs.netdna-ssl.com/wp-content/uploads/2014/02/Dashboard-announcement.png?58e47e',
    'class'     => 'announcement',
    'url'       => 'https://wedevs.com/products/plugins/dokan/'
);

// header( 'Content-Type: application/json; charset=UTF-8' );

// echo json_encode( $pro_features );